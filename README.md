# Giovanna Borges Bottino
## Matricula: 17/0011267 
### EP1 - OO 2019.1 (UnB - Gama)

Este projeto consiste em criar um jogo em C++ similar ao game Battleship (ou batalha naval), onde todas suas instruções, dicas e requisitos para a avaliação estão sendo descritos na [wiki](https://gitlab.com/oofga/eps/eps_2019_1/ep1/wikis/home) do repositório.

## Instruções de execução

A construção e execução do projeto funciona da seguinte maneira:

Para construir os objetos definidos pelo projeto, juntamente com o binário para realizar a execução:
```
$ make
```

Para executar o binário criado e iniciar o programa:
```
$ make run
```

Lembre-se sempre de criar o objeto dos novos, limpando os antigos:
```
$ make clean
```