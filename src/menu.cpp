#include "menu.hpp"
#include "map.hpp"
#include "boat.hpp"
#include "submarine.hpp"
#include "carrier.hpp"
#include "canoe.hpp"
#include "pontuation.hpp"

#include <unistd.h>

#include <iostream>

Menu::Menu(){
  //inicializa
   option = 0;
   Map map_1;
   Map map_2;
   readFile(1);
   Pontuation pontuation_1;
   Pontuation pontuation_2;
}
//Menu inicial
int Menu::Initial(){
   //Print Menu on screen
   cout << "*****************************" << '\n';
   cout << "Menu" << endl;
   cout << "<1> Start" << '\n';
   cout << "<2> Choose Map" << '\n';
   cout << "<3> Exit" << '\n';
   cout << "Your option:" << '\n';
   cin >> option;
   //Select Option
   switch (option) {
    case 1:
      //zera as pontuações
      pontuation_1.initialPontuation();
      pontuation_2.initialPontuation();
      //inicializa o número de barcos
      pontuation_1.setInitialNumberBoats(pontuation_1.getNumberBoats());
      pontuation_2.setInitialNumberBoats(pontuation_2.getNumberBoats());
      //inicia o jogo
      startGame(map_1,map_2);
      break;
    case 2:
      ChooseMap();
      break;
    case 3:
      exit(0);
      break;
    default:
      cout << "Try again" << '\n';
      Initial();
      break;
   } //END Select Option
   return 0;
} //END Menu

Menu::~Menu(){
} //END ~Menu

//Menu para escolher mapa
void Menu::ChooseMap(){
  //Print Menu on Screen
  cout << "*****************************" << '\n';
  cout << "Map" << endl;
  cout << "<1> Map 1 - 13x13" << endl;
  cout << "<2> Map 2 - 5x5" << endl;
  cout << "<3> Map 3 - 17x17" << endl;
  cout << "<4> Random" << endl;
  cout << "<5> Go back" << endl;
  cout << "Your option:" << '\n';
  //Scanei a opção
  int opt;
  cin >> opt;
  // Select option
 if (opt == 5){
    //Go back to menu
    usleep(3000000);
    Initial();
  } else if(opt == 4){
    //random map
    opt = rand() % 3 + 2;
  }else if (opt< 1 || opt>6){
    //no option select
    cout << "*****************************" << '\n';
    cout << "Try again" << '\n';
    usleep(3000000);
    ChooseMap();
  } //END if else
  //Seleciona o número do mapa

  this->map_1.setNumberMap(opt);
  this->map_2.setNumberMap(opt);
  //lê o arquivo do mapa
  try {
        readFile(opt);
    } catch (exception e) {
        cout << "Failed reading file\n";
    }

  cout << "*****************************" << '\n';
  cout << "You've choose Map " << opt  << '\n';
  //retorna ao menu inicial
  usleep(3000000);
  Initial();
} //END ChooseMap

//Função que faz o jogo funcionar
void Menu::startGame(Map map_1,Map map_2){
  while(1){
    cout << "*****************************" << '\n';
    cout << "Player 1" << "\t" << "Pontuation: " << pontuation_1.getPontuation() << endl;
    //Print Map on Screen
    map_1.printMap();
    //print Menu on Screen
    gameOption();
    //Verifica se as coordenadas são possiveis no mapa
    int x = 20;
    int y = 20;
    while (x > map_1.getSize() || y > map_1.getSize()){
      cin >> x >> y;
      if (x > map_1.getSize() || y > map_1.getSize()){
        cout << "Please, insert a number between 0 and " << map_1.getSize() -1 << endl;
      }
    }

    //Atira no local necessario
    map_1.shotMatrix(x, y);
    //Atualiza mapa e pontuação
    pontuation_1.setNumberBoats(map_1.getNumBoat());
    pontuation_1.controlPontuation();
    cout << "*****************************" << '\n';
    cout << "Player 1" << "\t" << "Pontuation: " << pontuation_1.getPontuation() << endl;
    //Printa o mapa na tela
    map_1.printMap();

    usleep(3000000);
    cout << "*****************************" << '\n';
    cout << "Player 2" << "\t" << "Pontuation: " << pontuation_2.getPontuation() << endl;
    //verifica se acabou o jogo
    gameOver(pontuation_1);
    //Print Map on Screen
    map_2.printMap();
    //print Menu on Screen
    gameOption();
    //Verifica se as coordenadas são possiveis no mapa
    x = 20;
    y = 20;
    while (x > map_1.getSize() || y > map_1.getSize()){
      cin >> x >> y;
      if (x > map_1.getSize() || y > map_1.getSize()){
        cout << "Please, insert a number between 0 and " << map_1.getSize() -1 << endl;
      }
    }
    //Envia as coodenadas a matrix e imprime novamente
    map_2.shotMatrix(x, y);
    //Atualiza a pontuação e o mapa
    pontuation_2.setNumberBoats(map_2.getNumBoat());
    pontuation_2.controlPontuation();
    cout << "*****************************" << '\n';
    cout << "Player 2" << "\t" << "Pontuation: " << pontuation_2.getPontuation() << endl;
    //verifica se acabou o jogo
    gameOver(pontuation_2);
    //print map on screen
    map_2.printMap();

    usleep(3000000);
    cout << "*****************************" << '\n';
    // if (){
    //   break;
    // }
  } //END while
} //END

//Opções do Menu do jogo
void Menu:: gameOption(){
  //Print Menu on Screen
  cout << "Game Option:" << endl;
  cout << "<1> Go back" << endl;
  cout << "<2> Attack!" << endl;
  //Scnei opção
  cout << "Your option:" << '\n';
  int opt = 0;
  cin >> opt;
  // Select option
  //Enquando nenhum opção descente for escolhida continuara a perguntar
  while (opt != 1 && opt != 2){
    cout << "*****************************" << '\n';
    cout << "Try again" << '\n';
    cout << "Your option:" << '\n';
    cin >> opt;
  } //END While Option
  if(opt == 1){
    //Volta para o menu
    Initial();
  } else if (opt == 2){
     //Opção para atacar
     cout << "*****************************" << '\n';
     cout << "For attack you've to write the coordinates:" << '\n';
   }
}

//Função para dar Game Over
int Menu:: gameOver(Pontuation p){
  if (p.getNumberBoats()==0){
    cout << "Congrats!!\t Player " << p.getPlayer() << "you've won!" << endl;
    cout << "With " << p.getPontuation() << " points!" << endl;
    return 0;
  } else {
    return 1;
  }
}
//Função para ler o arquivo
void Menu::readFile (int n){
   int x, y;
   int numC, numS, numP;

   string line;
   //Caminho e leitura do arquivo
   string path = "doc/map_" + to_string(n) + ".txt";
   ifstream myfile (path);
   //inicila a matrix de posição
   map_1.initialPosition();
   map_2.initialPosition();
   //Leitura do arquivo
   if (myfile.is_open()){
         //Quantidade de canoas
         getline (myfile,line);
         numC = atoi(line.c_str());
         //Loop para a quantidade de canoas
         for (int h=0; h<numC; h++){
          Canoe b;

          getline (myfile,line);
          x = atoi(line.c_str());
          b.setCoordX(x);

          getline (myfile,line);
          y = atoi(line.c_str());
          b.setCoordY(y);

          getline (myfile,line);
          b.setDirection(line);

          map_1.boatPosition(b);
         }

         //Quantidade de submarinos
         getline (myfile,line);
         numS = atoi(line.c_str());

         //Loop para a quantidade de submarinos
         for (int h=0; h<numS; h++){
             Submarine s;

             getline (myfile,line);
             x = atoi(line.c_str());
             s.setCoordX(x);

             getline (myfile,line);
             y = atoi(line.c_str());
             s.setCoordY(y);

             getline (myfile,line);
             s.setDirection(line);

             map_1.boatPosition(s);
         }

         //Quantidade de porta aviões
         getline (myfile,line);
         numP = atoi(line.c_str());

         //Loop para a quantidade de porta avioes
         for (int h=0; h<numP; h++){
           Carrier c;

           getline (myfile,line);
           x = atoi(line.c_str());
           c.setCoordX(x);

           getline (myfile,line);
           y = atoi(line.c_str());
           c.setCoordY(y);

           getline (myfile,line);
           c.setDirection(line);

           map_1.boatPosition(c);
         }
         // Começa a trabalhar a pontuação
         pontuation_1.setNumberBoats (numC + numS * 2 + numP * 4);
         map_1.setNumBoat(numC + numS * 2 + numP * 4);
          //Quantidade de canoas
           getline (myfile,line);
           numC = atoi(line.c_str());
           //Loop para a quantidade de canoas
           for (int h=0; h<numC; h++){
             Canoe b;

            getline (myfile,line);
            x = atoi(line.c_str());
            b.setCoordX(x);

            getline (myfile,line);
            y = atoi(line.c_str());
            b.setCoordY(y);

            getline (myfile,line);
            b.setDirection(line);

            map_2.boatPosition(b);
           }

           //Quantidade de submarinos
           getline (myfile,line);
           numS = atoi(line.c_str());
           //Loop para a quantidade de submarinos
           for (int h=0; h<numS; h++){
             Submarine s;

               getline (myfile,line);
               x = atoi(line.c_str());
               s.setCoordX(x);

               getline (myfile,line);
               y = atoi(line.c_str());
               s.setCoordY(y);

               getline (myfile,line);
               s.setDirection(line);

               map_2.boatPosition(s);
           }

           //Quantidade de porta aviões
           getline (myfile,line);
           numP = atoi(line.c_str());
           //Loop para a quantidade de porta avioes
           for (int h=0; h<numP; h++){
             Carrier c;

             getline (myfile,line);
             x = atoi(line.c_str());
             c.setCoordX(x);

             getline (myfile,line);
             y = atoi(line.c_str());
             c.setCoordY(y);

             getline (myfile,line);
             c.setDirection(line);

             map_2.boatPosition(c);
           }

           pontuation_2.setNumberBoats (numC + numS * 2 + numP * 4);
           map_2.setNumBoat(numC + numS * 2 + numP * 4);
   }
   else cout << "Unable to open file" << endl;
} // End readFile
