#include "boat.hpp"

#include <iostream>

Boat::Boat(){
  coordenadas_xi = 0;
  coordenadas_yi = 0;
  size = 0;
  type = 0;
  mov_x = 0;
  mov_y = 0;
}
Boat::~Boat(){

}
//Set e get coordenadas_xi
void Boat::setCoordX(int n){
  coordenadas_xi = n;
}
int Boat::getCoordX(){
  return coordenadas_xi;
}
//Set e get coordenadas_yi
void Boat::setCoordY(int n){
    coordenadas_yi = n;
}
int Boat::getCoordY(){
  return coordenadas_yi;
}
//set e get size
void Boat::setSize(int s){
  size = s;
}
int Boat::getSize(){
  return size;
}
//set e get amount
void Boat::setType(int a){
  type = a;
}
int Boat::getType(){
  return type;
}

void Boat::setMov_x(int x){
  mov_x = x;
}
int Boat::getMov_x(){
  return mov_x;
}

void Boat::setMov_y(int y){
  mov_y = y;
}
int Boat::getMov_y(){
  return mov_y;
}

//set e get mov_x e mov_y
void Boat::setDirection(string n){
  if(n == "cima"){
    mov_x = 0;
    mov_y = -1;
  }else if(n == "baixo"){
    mov_x = 0;
    mov_y = 1;
  }else if (n == "direita") {
    mov_x = 1;
    mov_y = 0;
  }else if (n == "esquerda"){
    mov_x = -1;
    mov_y = 0;
  }else if(n == "nada"){
    mov_x = 0;
    mov_y = 0;
  }
}
int Boat::getDirectionX(){
  return mov_x;
}
int Boat::getDirectionY(){
  return mov_y;
}
