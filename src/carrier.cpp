#include "carrier.hpp"

#include <iostream>

Carrier::Carrier(){
  setSize(4);
  setType(3);
}
Carrier::~Carrier(){

}

//set e get mov_x e mov_y
void Carrier::setDirection(string n){
  if(n == "cima"){
    setMov_x(0);
    setMov_y(-3);
  }else if(n == "baixo"){
    setMov_x(0);
    setMov_y(3);
  }else if (n == "direita") {
    setMov_x(3);
    setMov_y(0);
  }else if (n == "esquerda"){
    setMov_x(-3);
    setMov_y(0);
  }
}
