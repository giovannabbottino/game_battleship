#include "map.hpp"
#include "boat.hpp"
#include "submarine.hpp"
#include "carrier.hpp"
#include "canoe.hpp"

#include <iostream>
#include <string>

Map::Map(){
  setNumberMap(1);
  numBoat = 0;
} //END Map

Map::~Map(){
} //END ~Map

//Função para mudar o tamanho do mapa
void Map:: setNumberMap(int number){
  //Varivael que define tamanho do mapa
  int n=0;
  //Modifica para o mapa escolhido
  if (number==1){
    n = 13;
  } else if (number == 2){
    n = 5;
  } else if (number == 3){
    n = 17;
  }
  //Modifica os atributos
  this->numberColumn = n;
  this->numberLine = n;

  //zera uma nova matrix
  initialMatrix();
  initialPosition();
}//END setNumberMap
//Get do tamanho do mapa
int Map:: getSize(){
  return numberLine;
}

//Função para iniciar a matrix de tiros
void Map:: initialMatrix(){
  for (int i=0; i<getSize(); i++){
    for(int j=0; j<getSize(); j++){
        this->matrix[i][j] = 0;
    } //END for J
  } // END for i
}// END initialMatri

//Função para atira na matrix de tiros
void Map:: shotMatrix(int x, int y){
  while (this->matrix[x][y] == 1){
      cout << "You already had shot here!" << endl << "Try again!:" << endl;
      cin >> x >> y;
  }
  if (getPosition(x, y) == 1 || getPosition(x, y) == 0){
    this->matrix[x][y] = 1;
    if (getPosition(x, y) == 1){
        setNumBoat(getNumBoat() - 1);
    }
  } else if (getPosition(x, y) == 2){
    this->positionMatrix[x][y] = 1;
    this->matrix[x][y] = 2;
  } else if (getPosition(x, y) == 3){
    this->matrix[x][y] = rand() % 2 + 0;
    if (this->matrix[x][y] == 1){
        setNumBoat(getNumBoat() - 1);
    }
  }
}

//Função para iniciar a matrix de barcos
void Map:: initialPosition(){
  for (int i=0; i<getSize(); i++){
    for(int j=0; j<getSize(); j++){
        setPosition(i, j, 0);
    } //END for J
  } // END for i
} //END initialPosition

//Função para colocar um barco na posição
void Map:: boatPosition (Boat b){
  if (b.getMov_x() < 0 || b.getMov_y()< 0 ){
    for(int i = b.getCoordX(); i>=b.getCoordX() + b.getMov_x(); i--){
      for( int j= b.getCoordY(); j>=b.getCoordY() + b.getMov_y(); j--){
          setPosition(i, j, b.getType());
      }
    }
  } else if(b.getMov_x() >= 0 || b.getMov_y() >= 0 ){
    for(int i = b.getCoordX(); i<=b.getCoordX() + b.getMov_x(); i++){
      for( int j= b.getCoordY(); j<=b.getCoordY() + b.getMov_y(); j++){
          setPosition(i, j, b.getType());
      }
    }
  }
}

//Função set e get para o valor da posição
int Map:: getPosition(int x, int y){
  return this->positionMatrix[x][y];
}
void Map:: setPosition(int x, int y, int v){
    this->positionMatrix[x][y] = v;
}

//Função para imprimir o mapa
void Map:: printMap(){
  //Imprime as coordenadas x
  cout << "\t";
  for(int h =0 ; h < getSize(); h++){
      cout << h << "\t";
  }
  cout << '\n';

  for( int i= 0; i< getSize(); ++i){
    //Imprime as cordanadas y
    cout << i << "\t";
    //Imprime o mapa
    for(int j= 0; j< getSize(); ++j){
      cout << checkMap(i, j) << "\t";
    }//END Column
      cout << '\n';
  }//END Line
}

//Função para decidir qual caracter imprimir
string Map:: checkMap(int x, int y){
  //Por definição vai imprimir ~
  string place = "~";
  //Se no local já houve um tiro
  if (this->matrix[x][y] == 1){
    if ( getPosition(x, y) == 0){
        place = "V";
    } else if ( getPosition(x, y) == 1 || getPosition(x, y) == 2 || getPosition(x, y) == 3 ){
        place = "[]";
    }
  } else if (this->matrix[x][y] == 2){
    place = "!";
  }
  return place;
} //END checkMap

void Map:: imprimeMatrix(){
  for (int i=0; i<getSize(); i++){
    for(int j=0; j<getSize(); j++){
        cout << matrix[i][j];
    } //END for J
    cout << endl;
  } // END for i
}

void Map:: imprimePositionMatrix(){
  for (int i=0; i<getSize(); i++){
    for(int j=0; j<getSize(); j++){
        cout << positionMatrix[i][j];
    } //END for J
    cout << endl;
  } // END for i
}
//Set e get número de barcos
void Map:: setNumBoat(int b){
  this->numBoat = b;
}
int Map:: getNumBoat(){
  return this->numBoat;
}
