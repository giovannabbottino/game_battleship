#include "pontuation.hpp"

#include <iostream>

Pontuation::Pontuation(){
  numberBoats = 0;
  pontuation = 0;
  player = 0;
  savePontuation = 0;
}

Pontuation::~Pontuation(){
}

void Pontuation::initialPontuation(){
  this->pontuation = 0;
}
void Pontuation::setPontuation(int p){
  this->pontuation = p;
}
int Pontuation::getPontuation(){
  return this->pontuation;
}

void Pontuation::controlPontuation(){
  int final= 0;
  final= getInitialNumberBoats() * 100 - ((getInitialNumberBoats() - (getInitialNumberBoats() - getNumberBoats())) * 100);
  setPontuation(final);
}

void Pontuation:: setNumberBoats(int b){
  setSavePontuation(getNumberBoats());
  this->numberBoats = b;
}
int Pontuation::getNumberBoats(){
  return this->numberBoats;
}

void Pontuation::setInitialNumberBoats(int b){
  this-> initialNumber = b;
}
int Pontuation::getInitialNumberBoats(){
  return this->initialNumber;
}

void Pontuation:: setPlayer(int p){
  this->player = p;
}
int Pontuation:: getPlayer(){
  return this->player;
}

void Pontuation:: setSavePontuation(int i){
  this->savePontuation = i;
}
int Pontuation:: getSavePontuation(){
  return this->savePontuation;
}
