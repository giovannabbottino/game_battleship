#ifndef MENU_HPP
#define MENU_HPP

#include <string>
#include "map.hpp"
#include "pontuation.hpp"

using namespace std;

class Menu {

// Atributos
private:
   int option;
   Map map_1;
   Map map_2;
   Pontuation pontuation_1;
   Pontuation pontuation_2;

// Métodos
public:
   Menu();
   ~Menu();

   int Initial();
   void ChooseMap();

   void startGame(Map m1, Map m2);
   int gameOver(Pontuation p);

   void readFile(int i);

   void gameOption();
};

#endif
