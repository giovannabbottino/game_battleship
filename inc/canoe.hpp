#ifndef CANOE_HPP
#define CANOE_HPP

#include <string>
#include "boat.hpp"

using namespace std;

class Canoe : public Boat {

// Atributos
private:

// Métodos
public:
   Canoe();
   ~Canoe();
};


#endif
