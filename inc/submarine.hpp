#ifndef SUBMARINE_HPP
#define SUBMARINE_HPP

#include <string>
#include "boat.hpp"

using namespace std;

class Submarine : public Boat {

// Atributos
private:

// Métodos
public:
   Submarine();
   ~Submarine();
};


#endif
