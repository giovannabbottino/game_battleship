#ifndef BOAT_HPP
#define BOAT_HPP

#include <string>
#include <bits/stdc++.h>

using namespace std;

class Boat {

// Atributos
private:
  int coordenadas_xi;
  int coordenadas_yi;
  int size;
  int type;
  int mov_x;
  int mov_y;

// Métodos
public:
   Boat();
   ~Boat();

  void setCoordX(int x);
  int getCoordX();

  void setCoordY(int y);
  int getCoordY();

  void setSize(int s);
  int getSize();

  void setType(int a);
  int getType();

  void setMov_x(int x);
  int getMov_x();

  void setMov_y(int y);
  int getMov_y();

  void setDirection(string n);
  int getDirectionX();
  int getDirectionY();
};

#endif
