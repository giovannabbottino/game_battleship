#ifndef PONTUATION_HPP
#define PONTUATION_HPP

#include <bits/stdc++.h>

using namespace std;

class Pontuation {

// Atributos
private:
  int numberBoats;
  int initialNumber;
  int pontuation;
  int player;
  int savePontuation;
// Métodos
public:
   Pontuation();
   ~Pontuation();

   void initialPontuation();
   void setPontuation(int p);
   int getPontuation();
   void controlPontuation();

   void setNumberBoats(int b);
   int getNumberBoats();

   void setInitialNumberBoats(int b);
   int getInitialNumberBoats();

   void setPlayer(int p);
   int getPlayer();

   void setSavePontuation(int i);
   int getSavePontuation();
};

#endif
