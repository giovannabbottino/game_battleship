#ifndef CARRIER_HPP
#define CARRIER_HPP

#include <string>
#include "boat.hpp"

using namespace std;

class Carrier : public Boat {

// Atributos
private:

// Métodos
public:
   Carrier();
   ~Carrier();

   void setDirection(string n);
};


#endif
