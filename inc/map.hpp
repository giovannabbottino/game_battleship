#ifndef MAP_HPP
#define MAP_HPP

#include <bits/stdc++.h>

#include "boat.hpp"

using namespace std;

class Map {

// Atributos
private:
  int numberColumn;
  int numberLine;
  int matrix[20][20];
  int positionMatrix[20][20];
  int numBoat;
// Métodos
public:
   Map();
   ~Map();

   //Funções de tamanho
   void setNumberMap(int numb);
   int getSize();

   //Matrix
   void initialMatrix();
   void shotMatrix(int x, int y);

   //PositionMatrix
   void initialPosition();
   void boatPosition (Boat b);
   int getPosition(int x, int y);
   void setPosition(int x, int y, int v);

   //Funções de imprimir
   void printMap();
   string checkMap(int l, int c);
   void imprimePositionMatrix();
   void imprimeMatrix();

   //Funções para controlar os barcos
   void setNumBoat(int b);
   int getNumBoat();
};

#endif
